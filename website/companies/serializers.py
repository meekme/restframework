from rest_framework import serializers
from .models import Stock,Share

class StockSerializer(serializers.ModelSerializer):

	def validate_close(self,data):
		import ipdb;ipdb.set_trace()
		if data['open'] > data['close']:
			raise serializers.ValidationError("Open must be less than close")
		return data

	class Meta:
		model = Stock
		# fields = ('ticker','volume')
		fields = '__all__'

class ShareSerializer(serializers.ModelSerializer):

	class Meta:
		model = Share
		fields = '__all__'