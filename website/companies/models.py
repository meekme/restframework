from __future__ import unicode_literals
from django.db import models

class Stock(models.Model):
	ticker = models.CharField(max_length=10)
	open = models.FloatField()
	close = models.FloatField()
	volume = models.IntegerField()

	def __str__(self):
		return self.ticker

class Share(models.Model):
	ticker = models.CharField(max_length=10)
	bought = models.FloatField()
	sold = models.FloatField()

	def __str__(self):
		return self.ticker