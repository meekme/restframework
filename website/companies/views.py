from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import ListAPIView,RetrieveAPIView,UpdateAPIView,DestroyAPIView,CreateAPIView
from .models import Stock,Share
from .serializers import StockSerializer,ShareSerializer

# Get all data
class StockList(ListAPIView):
	queryset = Stock.objects.all()
	serializer_class = StockSerializer

# Get a single data by id
class StockRetrieve(RetrieveAPIView):
	queryset = Stock.objects.all()
	serializer_class = StockSerializer

# Update a stock 
class StockUpdate(UpdateAPIView):
	queryset = Stock.objects.all()
	serializer_class = StockSerializer

# Delete a stock
class StockDelete(DestroyAPIView):
	queryset = Stock.objects.all()
	serializer_class = StockSerializer

# Create a stock
class StockCreate(CreateAPIView):
	queryset = Stock.objects.all()
	serializer_class = StockSerializer

class ShareList(ListAPIView):
	queryset = Share.objects.all()
	serializer_class = ShareSerializer

class ShareRetrieve(RetrieveAPIView):
	queryset = Share.objects.all()
	serializer_class = ShareSerializer