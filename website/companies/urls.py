from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$',views.StockList.as_view(),name='stock-index'),
	url(r'^(?P<pk>\d+)/$',views.StockRetrieve.as_view(),name='stock-detail'),
	url(r'^(?P<pk>\d+)/edit/$',views.StockUpdate.as_view(),name='stock-update'),
	url(r'^(?P<pk>\d+)/delete/$',views.StockDelete.as_view(),name='stock-delete'),
	url(r'^create/$',views.StockCreate.as_view(),name='stock-create'),

	url(r'^share/$',views.ShareList.as_view(),name='share-index'),
	url(r'^share/(?P<pk>\d+)/$',views.ShareRetrieve.as_view(),name='share-detail')
]